// TODO: sharedに置いてバックエンドのと統合したい
export const iso639Langs1 = {
	af: {
		nativeName: "Afrikaans",
	},
	ak: {
		nativeName: "Tɕɥi",
	},
	ar: {
		nativeName: "العربية",
		rtl: true,
	},
	ay: {
		nativeName: "Aymar aru",
	},
	az: {
		nativeName: "Azərbaycan dili",
	},
	be: {
		nativeName: "Беларуская",
	},
	bg: {
		nativeName: "Български",
	},
	bn: {
		nativeName: "বাংলা",
	},
	br: {
		nativeName: "Brezhoneg",
	},
	bs: {
		nativeName: "Bosanski",
	},
	ca: {
		nativeName: "Català",
	},
	cs: {
		nativeName: "Čeština",
	},
	cy: {
		nativeName: "Cymraeg",
	},
	da: {
		nativeName: "Dansk",
	},
	de: {
		nativeName: "Deutsch",
	},
	el: {
		nativeName: "Ελληνικά",
	},
	en: {
		nativeName: "English",
	},
	eo: {
		nativeName: "Esperanto",
	},
	es: {
		nativeName: "Español",
	},
	et: {
		nativeName: "eesti keel",
	},
	eu: {
		nativeName: "Euskara",
	},
	fa: {
		nativeName: "فارسی",
		rtl: true,
	},
	ff: {
		nativeName: "Fulah",
	},
	fi: {
		nativeName: "Suomi",
	},
	fo: {
		nativeName: "Føroyskt",
	},
	fr: {
		nativeName: "Français",
	},
	fy: {
		nativeName: "Frysk",
	},
	ga: {
		nativeName: "Gaeilge",
	},
	gd: {
		nativeName: "Gàidhlig",
	},
	gl: {
		nativeName: "Galego",
	},
	gn: {
		nativeName: "Avañe'ẽ",
	},
	gu: {
		nativeName: "ગુજરાતી",
	},
	gv: {
		nativeName: "Gaelg",
	},
	he: {
		nativeName: "עברית‏",
		rtl: true,
	},
	hi: {
		nativeName: "हिन्दी",
	},
	hr: {
		nativeName: "Hrvatski",
	},
	ht: {
		nativeName: "Kreyòl",
	},
	hu: {
		nativeName: "Magyar",
	},
	hy: {
		nativeName: "Հայերեն",
	},
	id: {
		nativeName: "Bahasa Indonesia",
	},
	is: {
		nativeName: "Íslenska",
	},
	it: {
		nativeName: "Italiano",
	},
	ja: {
		nativeName: "日本語",
	},
	jv: {
		nativeName: "Basa Jawa",
	},
	ka: {
		nativeName: "ქართული",
	},
	kk: {
		nativeName: "Қазақша",
	},
	kl: {
		nativeName: "kalaallisut",
	},
	km: {
		nativeName: "ភាសាខ្មែរ",
	},
	kn: {
		nativeName: "ಕನ್ನಡ",
	},
	ko: {
		nativeName: "한국어",
	},
	ku: {
		nativeName: "Kurdî",
	},
	kw: {
		nativeName: "Kernewek",
	},
	la: {
		nativeName: "Latin",
	},
	lb: {
		nativeName: "Lëtzebuergesch",
	},
	li: {
		nativeName: "Lèmbörgs",
	},
	lt: {
		nativeName: "Lietuvių",
	},
	lv: {
		nativeName: "Latviešu",
	},
	mg: {
		nativeName: "Malagasy",
	},
	mk: {
		nativeName: "Македонски",
	},
	ml: {
		nativeName: "മലയാളം",
	},
	mn: {
		nativeName: "Монгол",
	},
	mr: {
		nativeName: "मराठी",
	},
	ms: {
		nativeName: "Bahasa Melayu",
	},
	mt: {
		nativeName: "Malti",
	},
	my: {
		nativeName: "ဗမာစကာ",
	},
	no: {
		nativeName: "Norsk",
	},
	nb: {
		nativeName: "Norsk (bokmål)",
	},
	ne: {
		nativeName: "नेपाली",
	},
	nl: {
		nativeName: "Nederlands",
	},
	nn: {
		nativeName: "Norsk (nynorsk)",
	},
	oc: {
		nativeName: "Occitan",
	},
	or: {
		nativeName: "ଓଡ଼ିଆ",
	},
	pa: {
		nativeName: "ਪੰਜਾਬੀ",
	},
	pl: {
		nativeName: "Polski",
	},
	ps: {
		nativeName: "پښتو",
		rtl: true,
	},
	pt: {
		nativeName: "Português",
	},
	qu: {
		nativeName: "Qhichwa",
	},
	rm: {
		nativeName: "Rumantsch",
	},
	ro: {
		nativeName: "Română",
	},
	ru: {
		nativeName: "Русский",
	},
	sa: {
		nativeName: "संस्कृतम्",
	},
	se: {
		nativeName: "Davvisámegiella",
	},
	sh: {
		nativeName: "српскохрватски",
	},
	si: {
		nativeName: "සිංහල",
	},
	sk: {
		nativeName: "Slovenčina",
	},
	sl: {
		nativeName: "Slovenščina",
	},
	so: {
		nativeName: "Soomaaliga",
	},
	sq: {
		nativeName: "Shqip",
	},
	sr: {
		nativeName: "Српски",
	},
	su: {
		nativeName: "Basa Sunda",
	},
	sv: {
		nativeName: "Svenska",
	},
	sw: {
		nativeName: "Kiswahili",
	},
	ta: {
		nativeName: "தமிழ்",
	},
	te: {
		nativeName: "తెలుగు",
	},
	tg: {
		nativeName: "забо́ни тоҷикӣ́",
	},
	th: {
		nativeName: "ภาษาไทย",
	},
	tr: {
		nativeName: "Türkçe",
	},
	tt: {
		nativeName: "татарча",
	},
	uk: {
		nativeName: "Українська",
	},
	ur: {
		nativeName: "اردو",
		rtl: true,
	},
	uz: {
		nativeName: "O'zbek",
	},
	vi: {
		nativeName: "Tiếng Việt",
	},
	xh: {
		nativeName: "isiXhosa",
	},
	yi: {
		nativeName: "ייִדיש",
		rtl: true,
	},
	zh: {
		nativeName: "中文",
	},
	zu: {
		nativeName: "isiZulu",
	},
};

export const iso639Langs3 = {
	ach: {
		nativeName: "Lwo",
	},
	ady: {
		nativeName: "Адыгэбзэ",
	},
	cak: {
		nativeName: "Maya Kaqchikel",
	},
	chr: {
		nativeName: "ᏣᎳᎩ (tsalagi)",
	},
	dsb: {
		nativeName: "Dolnoserbšćina",
	},
	fil: {
		nativeName: "Filipino",
	},
	hsb: {
		nativeName: "Hornjoserbšćina",
	},
	kab: {
		nativeName: "Taqbaylit",
	},
	mai: {
		nativeName: "मैथिली, মৈথিলী",
	},
	tlh: {
		nativeName: "tlhIngan-Hol",
	},
	tok: {
		nativeName: "Toki Pona",
	},
	yue: {
		nativeName: "粵語",
	},
	nan: {
		nativeName: "閩南語",
	},
};

export const langmapNoRegion = Object.assign({}, iso639Langs1, iso639Langs3);

export const iso639Regional = {
	"zh-hans": {
		nativeName: "中文（简体）",
	},
	"zh-hant": {
		nativeName: "中文（繁體）",
	},
};

export const langmap = Object.assign({}, langmapNoRegion, iso639Regional);

/**
 * @see https://github.com/komodojp/tinyld/blob/develop/docs/langs.md
 */
export const supportedLangs: Record<string, boolean> = {
	af: true,
	afr: true,
	am: true,
	amh: true,
	ber: true,
	rn: true,
	run: true,
	my: true,
	mya: true,
	id: true,
	ind: true,
	km: true,
	khm: true,
	tl: true,
	tgl: true,
	th: true,
	tha: true,
	vi: true,
	vie: true,
	zh: true,
	cmn: true,
	ja: true,
	jpn: true,
	ko: true,
	kor: true,
	bn: true,
	ben: true,
	gu: true,
	guj: true,
	hi: true,
	hin: true,
	kn: true,
	kan: true,
	ta: true,
	tam: true,
	te: true,
	tel: true,
	ur: true,
	urd: true,
	cs: true,
	ces: true,
	el: true,
	ell: true,
	la: true,
	lat: true,
	mk: true,
	mkd: true,
	sr: true,
	srp: true,
	sk: true,
	slk: true,
	be: true,
	bel: true,
	bg: true,
	bul: true,
	et: true,
	est: true,
	hu: true,
	hun: true,
	lv: true,
	lvs: true,
	lt: true,
	lit: true,
	pl: true,
	pol: true,
	ro: true,
	ron: true,
	ru: true,
	rus: true,
	uk: true,
	ukr: true,
	da: true,
	dan: true,
	fi: true,
	fin: true,
	is: true,
	isl: true,
	no: true,
	nob: true,
	sv: true,
	swe: true,
	nl: true,
	nld: true,
	en: true,
	eng: true,
	fr: true,
	fra: true,
	de: true,
	deu: true,
	ga: true,
	gle: true,
	it: true,
	ita: true,
	pt: true,
	por: true,
	es: true,
	spa: true,
	ar: true,
	ara: true,
	hy: true,
	hye: true,
	he: true,
	heb: true,
	kk: true,
	kaz: true,
	mn: true,
	mon: true,
	fa: true,
	pes: true,
	tt: true,
	tat: true,
	tr: true,
	tur: true,
	tk: true,
	tuk: true,
	yi: true,
	yid: true,
	eo: true,
	epo: true,
	tlh: true,
	vo: true,
	vol: true,
};
